/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gambit
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Verseny {
    
    static final long serialVersionUID = 42L;

    @XmlElement
    int helyszin;

    @XmlElement
    int szervezo;
    
    @XmlElement
    List<Merkozes> merkozes = new ArrayList<Merkozes>();

    public Verseny(){
        
    }
    
    public Verseny(int helyszin, int szervezo) {
        this.helyszin = helyszin;
        this.szervezo = szervezo;
    }
    
    public Verseny(int helyszin, int szervezo, Merkozes merkozes) {
        this.helyszin = helyszin;
        this.szervezo = szervezo;
        this.addMerkozes(merkozes);
    }

    public int getSzervezo() {
        return szervezo;
    }

    public void setSzervezo(int szervezo) {
        this.szervezo = szervezo;
    }

    public int getHelyszin() {
        return helyszin;
    }

    public void setHelyszin(int helyszin) {
        this.helyszin = helyszin;
    }
    
    public List<Merkozes> getMerkozes() {
        return merkozes;
    }

    public void addMerkozes(Merkozes merkozes) {
        this.merkozes.add(merkozes);
    }
}
