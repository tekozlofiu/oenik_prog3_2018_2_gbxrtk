/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author Gambit
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Merkozes {

    static final long serialVersionUID = 17L;
        
    @XmlElement
    int gyoztes;
    
    @XmlElement
    int vesztes;

    public Merkozes(int gyoztes, int vesztes) {
        this.gyoztes = gyoztes;
        this.vesztes = vesztes;
    }
    
    public int getGyoztes() {
        return gyoztes;
    }

    public void setGyoztes(int gyoztes) {
        this.gyoztes = gyoztes;
    }

    public int getVesztes() {
        return vesztes;
    }

    public void setVesztes(int vesztes) {
        this.vesztes = vesztes;
    }
}
