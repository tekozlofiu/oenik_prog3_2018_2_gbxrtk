# OENIK_PROG3_2018_2_GBXRTK

Versenyeredményeket nyilvántartó alkalmazás.

## Követelmények

[[HU] Project requirements](http://users.nik.uni-obuda.hu/prog3/_progtools/prog3_layers_requirements.pdf) - Féléves feladat követelmények

## Specifikáció

### Adatbázis felépítés

* szervezők ​(szervező id, vezetéknév, keresztnév, becenév, telefonszám, e-mail cím)
* játékosok (játékos id, vezetéknév, keresztnév, becenév, győzelmek száma, vereségek száma, Élő pont)
* helyszínek (helyszín id, név, irányítószám, helység, cím, házszám)
* versenyekKapcsoló (verseny id, dátum, helyszín id, szervező id)
* eredményekKapcsoló (verseny id, játékos id, ellenfél id, eredmény)

### Alapfunkciók

* Szervezők listázása / hozzáadása / módosítása / törlése
* Játékosok listázása / hozzáadása / módosítása / törlése
* Helyszínek listázása / hozzáadása / módosítása / törlése
* Versenyek listázása / hozzáadása / módosítása / törlése
* Eredmények listázása / hozzáadása / módosítása / törlése

### További funkciók

* Egy játékos összes ellenfelének listázása / rendezése
* Egy játékos legtöbbszöri legyőzője / legtöbbször legyőzőtt ellenfele
* Egy játékos leggyakoribb versenyhelyszíne
* Legnépszerűbb helyszín versenyek száma / egyedi indulók száma alapján
* Élő pont számítás
