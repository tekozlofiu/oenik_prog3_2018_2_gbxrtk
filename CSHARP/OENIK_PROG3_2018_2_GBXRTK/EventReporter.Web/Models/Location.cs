﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EventReporter.Web.Models
{
    public class Location
    {
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Név")]
        //[StringLength(24)]
        //[RegularExpression(@"^[A-ZÁÉÍÓÖŐÚÜŰ]+[a-zA-Záéíóöőúüű""'\s-]*$")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Irányítószám")]
        //[Range(1, 10000)]
        public int Zip { get; set; }

        [Required]
        [Display(Name = "Település")]
        //[StringLength(24)]
        //[RegularExpression(@"^[A-ZÁÉÍÓÖŐÚÜŰ]+[a-zA-Záéíóöőúüű""'\s-]*$")]
        public string City { get; set; }

        [Required]
        [Display(Name = "Cím")]
        //[StringLength(24)]
        //[RegularExpression(@"^[A-ZÁÉÍÓÖŐÚÜŰ]")]
        public string Address { get; set; }

        [Required]
        [Display(Name = "Házszám")]
        //[Range(1,1000)]
        public int Number { get; set; }
    }
}