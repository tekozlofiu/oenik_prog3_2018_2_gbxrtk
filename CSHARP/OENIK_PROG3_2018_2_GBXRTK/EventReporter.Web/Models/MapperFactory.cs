﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventReporter.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<EventReporter.Data.Helyszin, EventReporter.Web.Models.Location>().
                    ForMember(dest => dest.Id, map => map.MapFrom(src => src.Id)).
                    ForMember(dest => dest.Name, map => map.MapFrom(src => src.Nev)).
                    ForMember(dest => dest.Zip, map => map.MapFrom(src => src.Iranyitoszam)).
                    ForMember(dest => dest.City, map => map.MapFrom(src => src.Telepules)).
                    ForMember(dest => dest.Address, map => map.MapFrom(src => src.Cim)).
                    ForMember(dest => dest.Number, map => map.MapFrom(src => src.Hazszam));
            });
            return config.CreateMapper();
        }
    }
}