﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using EventReporter.Data;
using EventReporter.Logic;
using EventReporter.Repository;
using EventReporter.Web.Models;

namespace EventReporter.Web.Controllers
{
    public class LocationController : Controller
    {
        // GET: EventReporter
        public ActionResult Index()
        {
            var teszt = new Location()
            {
                Name = "AAA",
                City = "BBB",
                Address = "CCC",
                Zip = 1111,
                Number = 2,
            };

            return View("LocationView", teszt);
        }
    }
}