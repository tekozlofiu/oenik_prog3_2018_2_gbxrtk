﻿using AutoMapper;
using EventReporter.Data;
using EventReporter.Logic;
using EventReporter.Repository;
using EventReporter.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EventReporter.Web.Controllers
{
    public class LocationApiController : ApiController
    {
        public class ApiResult
        {
            public bool OperationResult { get; set; }
        }

        ILogic<Helyszin> logic;
        IMapper mapper;

        public LocationApiController()
        {
            var unitOfWork = new UnitOfWork<EventReporterContext>();
            logic = new Logic<Helyszin>(unitOfWork);
            mapper = MapperFactory.CreateMapper();
        }

        // GET api/LocationApi/all
        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Models.Location> GetAll()
        {
            var cars = logic.All();
            return mapper.Map<IList<Data.Helyszin>, IList<Models.Location>>(cars);
        }

        [ActionName("del")]
        [HttpGet]
        public ApiResult DeleteOneLocation(int id)
        {
            var result = logic.Get(x => x.Id.Equals(id)).FirstOrDefault();

            if (result is object)
            {
                logic.Delete(result);
            }

            return new ApiResult() { OperationResult = result is object };
        }

        [ActionName("add")]
        [HttpPost]
        public ApiResult AddOneLocation(Location location)
        {
            var helyszin = new Helyszin()
            {
                Nev = location.Name,
                Iranyitoszam = location.Zip,
                Telepules = location.City,
                Cim = location.Address,
                Hazszam = location.Number
            };

            logic.Add(helyszin);

            return new ApiResult() { OperationResult = true };
        }

        [ActionName("mod")]
        [HttpPost]
        public ApiResult ModifyOneLocation(Location location)
        {
            var result = logic.Get(x => x.Id.Equals(location.Id)).FirstOrDefault();

            if (result is object)
            {
                result.Nev = location.Name;
                result.Iranyitoszam = location.Zip;
                result.Telepules = location.City;
                result.Cim = location.Address;
                result.Hazszam = location.Number;

                logic.Update(result);
            }

            return new ApiResult() { OperationResult = result is object };
        }
    }
}
