﻿// <copyright file="Logic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EventReporter.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using EventReporter.Repository;

    /// <summary>
    /// Service layer business logic generic class.
    /// </summary>
    /// <typeparam name="TEntity">Data model type.</typeparam>
    public class Logic<TEntity> : ILogic<TEntity>
        where TEntity : class
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Logic{TEntity}"/> class.
        /// </summary>
        /// <param name="unitOfWork">Unit of work.</param>
        public Logic(IUnitOfWork unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
            this.Repository = unitOfWork.GetRepository<TEntity>();
        }

        /// <summary>
        /// Gets the unit of work
        /// </summary>
        protected IUnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets a repository
        /// </summary>
        protected IRepository<TEntity> Repository { get; }

        /// <summary>
        /// Add a new entitiy to the repository.
        /// </summary>
        /// <param name="entity">The new entity to be added.</param>
        public void Add(TEntity entity)
        {
            this.Repository.Add(entity);
            this.UnitOfWork.Save();
        }

        /// <summary>
        /// Fetches every entity from the repository.
        /// </summary>
        /// <returns>A list containing every entity.</returns>
        public List<TEntity> All()
        {
            return this.Repository.All().ToList<TEntity>();
        }

        /// <summary>
        /// Remove an entity from the repository.
        /// </summary>
        /// <param name="entity">The entity to be deleted.</param>
        public void Delete(TEntity entity)
        {
            this.Repository.Delete(entity);
            this.UnitOfWork.Save();
        }

        /// <summary>
        /// Fetch entities from the repository matching the given condition.
        /// </summary>
        /// <param name="condition">And expression that has to be matched.</param>
        /// <returns>A list of entities that are matching the condition.</returns>
        public List<TEntity> Get(Expression<Func<TEntity, bool>> condition)
        {
            return this.Repository.Get(condition).ToList<TEntity>();
        }

        /// <summary>
        /// Modify an entity in the repository.
        /// </summary>
        /// <param name="entity">The entity to be modified.</param>
        public void Update(TEntity entity)
        {
            this.Repository.Update(entity);
            this.UnitOfWork.Save();
        }
    }
}