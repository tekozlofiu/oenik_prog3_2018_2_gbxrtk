﻿// <copyright file="Java.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EventReporter.Logic
{
    using System;
    using System.Linq;
    using System.Xml.Linq;
    using EventReporter.Data;
    using EventReporter.Repository;

    /// <summary>
    /// Manages connection with a java end point
    /// </summary>
    public class Java
    {
        /// <summary>
        /// Connect to the java end point and download XML information.
        /// </summary>
        /// <param name="url">The servlet's URL.</param>
        /// <param name="unitOfWork">Unit of Work</param>
        public static void Connect(string url, IUnitOfWork unitOfWork)
        {
            XDocument xDoc = XDocument.Load(url);

            Console.WriteLine();

            var versenyek = xDoc.Elements("verseny").Select(
                x => new Verseny()
            {
                Helyszin = int.Parse(x.Element("helyszin").Value),
                Szervezo = int.Parse(x.Element("szervezo").Value),
                Idopont = DateTime.Now
            }).ToList();

            if (versenyek.Count > 0)
            {
                var versenyLogic = new Logic<Verseny>(unitOfWork);

                foreach (var verseny in versenyek)
                {
                    versenyLogic.Add(verseny);
                }

                Console.WriteLine("\nÚj verseny hozzáadva!", versenyek.Count);
                System.Threading.Thread.Sleep(1500);

                int lastId = versenyLogic.All().Max(x => x.Id);

                var merkozesek = xDoc.Root.Descendants("merkozes").Select(
                    x => new Merkozes()
                    {
                        Verseny = lastId,
                        Gyoztes = int.Parse(x.Element("gyoztes").Value),
                        Vesztes = int.Parse(x.Element("vesztes").Value),
                    }).ToList();

                var merkozesLogic = new Logic<Merkozes>(unitOfWork);

                foreach (var merkozes in merkozesek)
                {
                    merkozesLogic.Add(merkozes);
                }

                Console.WriteLine("{0} versenyeredmény feltöltve!", merkozesek.Count);
                System.Threading.Thread.Sleep(1500);
            }
            else
            {
                Console.WriteLine("Nincsenek új versenyeredmények");
            }
        }
    }
}
