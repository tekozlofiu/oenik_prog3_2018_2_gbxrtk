﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EventReporter.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;

    /// <summary>
    /// Generic interface for CRUD functionality implementation.
    /// </summary>
    /// <typeparam name="TEntity">a</typeparam>
    public interface ILogic<TEntity>
        where TEntity : class
    {
        /// <summary>
        /// Fetches every entity from the repository.
        /// </summary>
        /// <returns>A list containing every entity.</returns>
        List<TEntity> All();

        /// <summary>
        /// Add a new entitiy to the repository.
        /// </summary>
        /// <param name="entity">The new entity to be added.</param>
        void Add(TEntity entity);

        /// <summary>
        /// Remove an entity from the repository that matches the given condition.
        /// </summary>
        /// <param name="entity">The entity to be deleted.</param>
        void Delete(TEntity entity);

        /// <summary>
        /// Fetch entities from the repository matching the given condition.
        /// </summary>
        /// <param name="condition">And expression that has to be matched.</param>
        /// <returns>A list of entities that are matching the condition.</returns>
        List<TEntity> Get(Expression<Func<TEntity, bool>> condition);

        /// <summary>
        /// Modify an entity in the repository.
        /// </summary>
        /// <param name="entity">The entity to be modified.</param>
        void Update(TEntity entity);
    }
}