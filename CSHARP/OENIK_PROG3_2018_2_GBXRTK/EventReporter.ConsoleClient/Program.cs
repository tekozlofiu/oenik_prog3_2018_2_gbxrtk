﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace EventReporter.ConsoleClient
{
    public class Location
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Zip { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public int Number { get; set; }

        public override string ToString()
        {
            return $"#{Id} {Name} - {Zip}. {City}, {Address} {Number}";
        }
    }

    class Program
    {
        static void Main()
        {
            Console.WriteLine("Waiting...");
            Console.ReadKey();

            string urlBase = "http://localhost:63654/api/LocationApi/";

            using (var client = new HttpClient())
            {
                string json = client.GetStringAsync(urlBase + "all").Result;
                var list = JsonConvert.DeserializeObject<List<Location>>(json);

                foreach (var item in list)
                {
                    Console.WriteLine(item);
                }

                /* ADD test */

                var postData = new Dictionary<string, string>();

                postData.Add(nameof(Location.Name), "Teszt");
                postData.Add(nameof(Location.Zip), "1111");
                postData.Add(nameof(Location.City), "Város");
                postData.Add(nameof(Location.Address), "Cím");
                postData.Add(nameof(Location.Number), "22");

                string response = client
                    .PostAsync(urlBase + "add", new FormUrlEncodedContent(postData))
                    .Result
                    .Content
                    .ReadAsStringAsync()
                    .Result;

                Console.WriteLine("ADD: " + response);

                json = client.GetStringAsync(urlBase + "all").Result;
                Console.WriteLine("ALL: " + json);

                /* MOD test */

                var id = JsonConvert.DeserializeObject<List<Location>>(json).Single(x => x.Name.Equals("Teszt")).Id;

                postData = new Dictionary<string, string>();

                postData.Add(nameof(Location.Id), id.ToString());
                postData.Add(nameof(Location.Name), "Teszt");
                postData.Add(nameof(Location.Zip), "1119");
                postData.Add(nameof(Location.City), "VárosMod");
                postData.Add(nameof(Location.Address), "CímMod");
                postData.Add(nameof(Location.Number), "2229");

                response = client
                    .PostAsync(urlBase + "mod", new FormUrlEncodedContent(postData))
                    .Result
                    .Content
                    .ReadAsStringAsync()
                    .Result;

                Console.WriteLine("MOD: " + response);

                json = client.GetStringAsync(urlBase + "all").Result;
                Console.WriteLine("ALL: " + json);

                /* DEL test */
                response = client.GetStringAsync(urlBase + "del/" + id).Result;

                Console.WriteLine("DEL: " + response);

                json = client.GetStringAsync(urlBase + "all").Result;
                Console.WriteLine("ALL: " + json);
            }

            Console.ReadKey();
        }
    }
}
