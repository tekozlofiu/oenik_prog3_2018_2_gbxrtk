//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EventReporter.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Helyszin
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Helyszin()
        {
            this.Verseny = new HashSet<Verseny>();
        }

        [Key]
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Required]
        [Display(Name = "N�v")]
        //[StringLength(24)]
        //[RegularExpression(@"^[A-Z���������]+[a-zA-Z���������""'\s-]*$")]
        public string Nev { get; set; }

        [Required]
        [Display(Name = "Ir�ny�t�sz�m")]
        //[Range(1, 10000)]
        public int Iranyitoszam { get; set; }

        [Required]
        [Display(Name = "Telep�l�s")]
        //[StringLength(24)]
        //[RegularExpression(@"^[A-Z���������]+[a-zA-Z���������""'\s-]*$")]
        public string Telepules { get; set; }

        [Required]
        [Display(Name = "C�m")]
        //[StringLength(24)]
        //[RegularExpression(@"^[A-Z���������]")]
        public string Cim { get; set; }

        [Required]
        [Display(Name = "H�zsz�m")]
        //[Range(1,1000)]
        public int Hazszam { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Verseny> Verseny { get; set; }

        public override string ToString()
        {
            return Nev;
        }
    }
}
