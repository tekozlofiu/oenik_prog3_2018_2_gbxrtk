//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EventReporter.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Jatekos
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Jatekos()
        {
            this.Merkozes = new HashSet<Merkozes>();
            this.Merkozes1 = new HashSet<Merkozes>();
        }

        [Key]
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Vezetéknév")]
        public string Vezeteknev { get; set; }

        [Required]
        [Display(Name = "Kersztnév")]
        public string Keresztnev { get; set; }
        [StringLength(24)]

        [Required]
        [Display(Name = "Becenév")]
        public string Becenev { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Regisztráció dátuma")]
        public DateTime Regisztracio { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Születésidátum")]
        public DateTime Szuletes { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Merkozes> Merkozes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Merkozes> Merkozes1 { get; set; }

        public override string ToString()
        {
            return Vezeteknev + " " + Keresztnev + " (" + Becenev +")";
        }
    }
}
