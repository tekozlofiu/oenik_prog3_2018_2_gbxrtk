//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EventReporter.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Merkozes
    {
        [Key]
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Association("SzervezoFK", "Verseny", "Id")]
        [Display(Name = "Verseny")]
        public int Verseny { get; set; }

        [Association("GyoztesFK", "Gyoztes", "Id")]
        [Display(Name = "Gy�ztes")]
        public int Gyoztes { get; set; }

        [Association("VesztesFK", "Vesztes", "Id")]
        [Display(Name = "Vesztes")]
        public int Vesztes { get; set; }
    
        public virtual Jatekos Jatekos { get; set; }
        public virtual Jatekos Jatekos1 { get; set; }
        public virtual Verseny Verseny1 { get; set; }

        public override string ToString()
        {
            string result = string.Empty;
            result += Verseny1 != null ? Verseny1.Idopont.ToString("yyyy-MM-dd") + " | " : "n/a | ";
            result += Jatekos != null ? Jatekos.Becenev + " | " : "n/a | ";
            result += Jatekos1 != null ? Jatekos1.Becenev + " | " : "n/a | ";
            return result;
        }
    }
}