﻿CREATE TABLE szervezo
(
	szervezo_id	NUMERIC(20) NOT NULL,
	vezeteknev	VARCHAR(50),
	keresztnev	VARCHAR(50),
	email		VARCHAR(50) UNIQUE,
	telefon		VARCHAR(20) UNIQUE,
	CONSTRAINT szervezo_pk PRIMARY KEY(szervezo_id)
);

CREATE TABLE jatekos
(
	jatekos_id		NUMERIC(20) NOT NULL,
	becenev			VARCHAR(30) UNIQUE,
	regisztracio	DATETIME,
	szuletes		DATETIME,
	CONSTRAINT jatekos_pk PRIMARY KEY(jatekos_id)
);

CREATE TABLE helyszin
(
	helyszin_id		NUMERIC(20) NOT NULL,
	nev				VARCHAR(50) UNIQUE,
	telepules		VARCHAR(50),
	iranyitoszam	NUMERIC(4),
	cim				VARCHAR(50),
	hazszam			NUMERIC(4),
	CONSTRAINT helyszin_pk PRIMARY KEY(helyszin_id)
);

CREATE TABLE verseny
(
	verseny_id		NUMERIC(20) NOT NULL,
	szervezo_id		NUMERIC(20) NOT NULL,
	helyszin_id		NUMERIC(20) NOT NULL,
	idopont			DATETIME,
	CONSTRAINT verseny_pk PRIMARY KEY(verseny_id),
	CONSTRAINT szervezo_fk FOREIGN KEY(szervezo_id) REFERENCES szervezo(szervezo_id),
	CONSTRAINT helyszin_fk FOREIGN KEY(helyszin_id) REFERENCES helyszin(helyszin_id)
);

CREATE TABLE merkozes
(
	merkozes_id		NUMERIC(20) NOT NULL,
	verseny_id		NUMERIC(20) NOT NULL,
	gyoztes			NUMERIC(20) NOT NULL,
	vesztes 		NUMERIC(20) NOT NULL,
	CONSTRAINT merkozes_pk PRIMARY KEY(merkozes_id),
	CONSTRAINT verseny_fk FOREIGN KEY(verseny_id) REFERENCES verseny(verseny_id),
	CONSTRAINT gyoztes_fk FOREIGN KEY(gyoztes) REFERENCES jatekos(jatekos_id),
	CONSTRAINT vesztes_fk FOREIGN KEY(vesztes) REFERENCES jatekos(jatekos_id)
);

INSERT INTO szervezo VALUES (1, 'Kriston', 'Dávid', 'tekozlofiu@gmail.com', '06201234567');
INSERT INTO szervezo VALUES (2, 'Nagy', 'Péter', 'zombeee@gmail.com', '+36309876543');
INSERT INTO szervezo VALUES (3, 'Magyar', 'Máté', 'aquir@gmail.com', '0620/5554444');
INSERT INTO szervezo VALUES (4, 'Greguss', 'Szabolcs', 'gregcs@gmail.com', '70-222-33-44');

INSERT INTO jatekos VALUES (1, 'Gambit', '2016.03.08', '1986.06.09');
INSERT INTO jatekos VALUES (2, 'Guldor', '2018.04.09', '1990.03.01');
INSERT INTO jatekos VALUES (3, 'KeZso', '2012.05.10', '1984.07.21');
INSERT INTO jatekos VALUES (4, 'Petykó', '2017.06.11', '2003.11.16');

INSERT INTO helyszin VALUES (1, '10minutes', '1092', 'Budapest', 'Futó u.', '47');
INSERT INTO helyszin VALUES (2, 'Barcraft Nyugati', '1188', 'Budapest', 'Bajcsy-Zsilinszky út', '58');
INSERT INTO helyszin VALUES (3, ' Sárkánytűz', '1092', 'Budapest', 'Ferenc krt.', '40');
INSERT INTO helyszin VALUES (4, 'Gamer Box', '3530', 'Miskolc', 'Széchenyi utca', '78');

INSERT INTO verseny VALUES (1, '2018.10.21', 1, 1);
INSERT INTO verseny VALUES (2, '2018.10.21', 2, 2);
INSERT INTO verseny VALUES (3, '2018.10.21', 3, 1);
INSERT INTO verseny VALUES (4, '2018.10.21', 1, 4);

INSERT INTO merkozes VALUES (1, 1, 1);
INSERT INTO merkozes VALUES (2, 4, 2);
INSERT INTO merkozes VALUES (2, 2, 3);