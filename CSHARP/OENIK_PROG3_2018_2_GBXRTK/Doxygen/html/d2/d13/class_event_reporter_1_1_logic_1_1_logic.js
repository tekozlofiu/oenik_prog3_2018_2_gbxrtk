var class_event_reporter_1_1_logic_1_1_logic =
[
    [ "Logic", "d2/d13/class_event_reporter_1_1_logic_1_1_logic.html#a990ac4163051d096a048f5b4d0577a4e", null ],
    [ "Add", "d2/d13/class_event_reporter_1_1_logic_1_1_logic.html#a9d23cc5cc526cb57554afd5e8c93a3a1", null ],
    [ "All", "d2/d13/class_event_reporter_1_1_logic_1_1_logic.html#aa96edc53ebc96734a64f23630ace13ab", null ],
    [ "Delete", "d2/d13/class_event_reporter_1_1_logic_1_1_logic.html#a76988106370edbfcf77aaae153471882", null ],
    [ "Get", "d2/d13/class_event_reporter_1_1_logic_1_1_logic.html#ac14742c8729958a1c34afe6ef58f2017", null ],
    [ "Update", "d2/d13/class_event_reporter_1_1_logic_1_1_logic.html#a37ac4bb4baf4202ff0e4290007bd31fe", null ],
    [ "Repository", "d2/d13/class_event_reporter_1_1_logic_1_1_logic.html#a3e28228be83f1332557e674d19c7ead3", null ],
    [ "UnitOfWork", "d2/d13/class_event_reporter_1_1_logic_1_1_logic.html#a45b74a91bc7008fb3b3b0a34218b907b", null ]
];