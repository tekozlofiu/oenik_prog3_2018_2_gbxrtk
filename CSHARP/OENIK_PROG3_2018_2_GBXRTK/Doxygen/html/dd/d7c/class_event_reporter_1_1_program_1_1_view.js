var class_event_reporter_1_1_program_1_1_view =
[
    [ "View", "dd/d7c/class_event_reporter_1_1_program_1_1_view.html#a4414b2b2ccd7f33ae892c0e9d4b32d2c", null ],
    [ "DisplayOptions", "dd/d7c/class_event_reporter_1_1_program_1_1_view.html#a0546b903136591198ace7b81b8b9985f", null ],
    [ "Hozzáadás", "dd/d7c/class_event_reporter_1_1_program_1_1_view.html#a969e77abe18883cbb8e79d31c2b25e5c", null ],
    [ "Listázás", "dd/d7c/class_event_reporter_1_1_program_1_1_view.html#aa455c66df36c8fee159764aa4ca01e16", null ],
    [ "Módosítás", "dd/d7c/class_event_reporter_1_1_program_1_1_view.html#a20ba8c05a7cc3f1203411da50e801a41", null ],
    [ "Részletek", "dd/d7c/class_event_reporter_1_1_program_1_1_view.html#a2167ec1a9f0f07bed10797f4c9f6278a", null ],
    [ "Törlés", "dd/d7c/class_event_reporter_1_1_program_1_1_view.html#a3f4f8bcb00f313297521fca8e43de814", null ],
    [ "Logic", "dd/d7c/class_event_reporter_1_1_program_1_1_view.html#aa60e7aabfd8b2d4e41a8d702df568440", null ],
    [ "Options", "dd/d7c/class_event_reporter_1_1_program_1_1_view.html#a352cd14774c33b8a7e39e8a3a9bfc1c1", null ],
    [ "Properties", "dd/d7c/class_event_reporter_1_1_program_1_1_view.html#af6f82302a544f2b8db0d39cf494e4b94", null ],
    [ "Title", "dd/d7c/class_event_reporter_1_1_program_1_1_view.html#a86df664bced7fecabd9a5107dbb0f4dd", null ]
];