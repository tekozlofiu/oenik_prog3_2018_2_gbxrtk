var hierarchy =
[
    [ "DbContext", null, [
      [ "EventReporter.Data.EventReporterContext", "de/d54/class_event_reporter_1_1_data_1_1_event_reporter_context.html", null ]
    ] ],
    [ "EventReporter.Data.Helyszin", "d8/dc3/class_event_reporter_1_1_data_1_1_helyszin.html", null ],
    [ "EventReporter.Logic.ILogic< TEntity >", "db/d41/interface_event_reporter_1_1_logic_1_1_i_logic.html", [
      [ "EventReporter.Logic.Logic< TEntity >", "d2/d13/class_event_reporter_1_1_logic_1_1_logic.html", null ]
    ] ],
    [ "EventReporter.Repository.IRepository< TEntity >", "db/dae/interface_event_reporter_1_1_repository_1_1_i_repository.html", [
      [ "EventReporter.Repository.Repository< TEntity >", "d9/d0a/class_event_reporter_1_1_repository_1_1_repository.html", null ]
    ] ],
    [ "EventReporter.Repository.IUnitOfWork", "d5/da2/interface_event_reporter_1_1_repository_1_1_i_unit_of_work.html", [
      [ "EventReporter.Repository.UnitOfWork< TContext >", "dc/dc7/class_event_reporter_1_1_repository_1_1_unit_of_work.html", null ]
    ] ],
    [ "EventReporter.Data.Jatekos", "dc/d28/class_event_reporter_1_1_data_1_1_jatekos.html", null ],
    [ "EventReporter.Logic.Java", "d8/d27/class_event_reporter_1_1_logic_1_1_java.html", null ],
    [ "EventReporter.Logic.Logic< EventReporter.Logic.Tests.TestType >", "d2/d13/class_event_reporter_1_1_logic_1_1_logic.html", null ],
    [ "EventReporter.Logic.Tests.LogicTests", "d5/d4e/class_event_reporter_1_1_logic_1_1_tests_1_1_logic_tests.html", null ],
    [ "EventReporter.Data.Merkozes", "dc/d91/class_event_reporter_1_1_data_1_1_merkozes.html", null ],
    [ "EventReporter.Program.Program", "de/da7/class_event_reporter_1_1_program_1_1_program.html", null ],
    [ "Gbxrtk.EventReporter.Response", "da/d5b/class_gbxrtk_1_1_event_reporter_1_1_response.html", null ],
    [ "EventReporter.Data.Szervezo", "d5/de6/class_event_reporter_1_1_data_1_1_szervezo.html", null ],
    [ "EventReporter.Logic.Tests.TestType", "d4/d86/class_event_reporter_1_1_logic_1_1_tests_1_1_test_type.html", null ],
    [ "EventReporter.Data.Verseny", "d2/d5a/class_event_reporter_1_1_data_1_1_verseny.html", null ],
    [ "EventReporter.Program.View< TModel >", "dd/d7c/class_event_reporter_1_1_program_1_1_view.html", null ],
    [ "EventReporter.Program.View< Helyszin >", "dd/d7c/class_event_reporter_1_1_program_1_1_view.html", [
      [ "EventReporter.Program.ViewHelyszin", "d5/dfb/class_event_reporter_1_1_program_1_1_view_helyszin.html", null ]
    ] ],
    [ "EventReporter.Program.View< Jatekos >", "dd/d7c/class_event_reporter_1_1_program_1_1_view.html", [
      [ "EventReporter.Program.ViewJatekos", "d8/dfb/class_event_reporter_1_1_program_1_1_view_jatekos.html", null ]
    ] ],
    [ "EventReporter.Program.View< Merkozes >", "dd/d7c/class_event_reporter_1_1_program_1_1_view.html", [
      [ "EventReporter.Program.ViewMerkozes", "dd/d1a/class_event_reporter_1_1_program_1_1_view_merkozes.html", null ]
    ] ],
    [ "EventReporter.Program.View< Szervezo >", "dd/d7c/class_event_reporter_1_1_program_1_1_view.html", [
      [ "EventReporter.Program.ViewSzervezo", "d7/d0e/class_event_reporter_1_1_program_1_1_view_szervezo.html", null ]
    ] ],
    [ "EventReporter.Program.View< Verseny >", "dd/d7c/class_event_reporter_1_1_program_1_1_view.html", [
      [ "EventReporter.Program.ViewVerseny", "da/d10/class_event_reporter_1_1_program_1_1_view_verseny.html", null ]
    ] ],
    [ "EventReporter.Program.ViewJava", "d0/df4/class_event_reporter_1_1_program_1_1_view_java.html", null ],
    [ "EventReporter.Program.ViewMenu", "d4/d6d/class_event_reporter_1_1_program_1_1_view_menu.html", null ]
];