var class_event_reporter_1_1_logic_1_1_tests_1_1_logic_tests =
[
    [ "AddMethod_IsCalled_RepositoryMethod_IsAlsoCalled", "d5/d4e/class_event_reporter_1_1_logic_1_1_tests_1_1_logic_tests.html#a6ce2e91e6dda18951964a30dddf1f5e1", null ],
    [ "AllMethod_IsCalled_RepositoryMethod_IsAlsoCalled", "d5/d4e/class_event_reporter_1_1_logic_1_1_tests_1_1_logic_tests.html#a7ac1405b295e6a58b966fc7c4fdc32a6", null ],
    [ "AllMethod_Returns_IEnumerableCollectionOf_Four_GenericTypeObjects", "d5/d4e/class_event_reporter_1_1_logic_1_1_tests_1_1_logic_tests.html#a41e0616ad19fb334ed1a623699d5daa3", null ],
    [ "AllMethod_Returns_IEnumerableCollectionOf_GenericTypeObjects", "d5/d4e/class_event_reporter_1_1_logic_1_1_tests_1_1_logic_tests.html#a9e1dd46b331fcc46bca081a8bf9aeaba", null ],
    [ "DeleteMethod_IsCalled_RepositoryMethod_IsAlsoCalled", "d5/d4e/class_event_reporter_1_1_logic_1_1_tests_1_1_logic_tests.html#a2616d56943a4cf224c4791596c7c870e", null ],
    [ "GetMethod_CalledFor_MultipleValues_Returns_MultipleValue", "d5/d4e/class_event_reporter_1_1_logic_1_1_tests_1_1_logic_tests.html#ad5fbab39428d4a6401d7a8b9cf437146", null ],
    [ "GetMethod_CalledFor_SingleValue_Returns_SingleValue", "d5/d4e/class_event_reporter_1_1_logic_1_1_tests_1_1_logic_tests.html#a7f4d136b526fafb87d15466dc3cc73f5", null ],
    [ "GetMethod_CalledForWrongValue_ReturnsZero", "d5/d4e/class_event_reporter_1_1_logic_1_1_tests_1_1_logic_tests.html#adfd774faad9781e1ea0267597f1ff225", null ],
    [ "GetMethod_IsCalled_RepositoryMethod_IsAlsoCalled", "d5/d4e/class_event_reporter_1_1_logic_1_1_tests_1_1_logic_tests.html#a00d71c9abe1748b1bac13cfd27a88c94", null ],
    [ "SetUp", "d5/d4e/class_event_reporter_1_1_logic_1_1_tests_1_1_logic_tests.html#a65911fc07205df0d4eda79452a3a80f3", null ],
    [ "UpdateMethod_IsCalled_RepositoryMethod_IsAlsoCalled", "d5/d4e/class_event_reporter_1_1_logic_1_1_tests_1_1_logic_tests.html#a305ff664dafd5840d1e90bb098073ff3", null ]
];