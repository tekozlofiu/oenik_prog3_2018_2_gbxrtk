﻿// <copyright file="LogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EventReporter.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using EventReporter.Data;
    using EventReporter.Logic;
    using EventReporter.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Arrange
    /// </summary>
    [TestFixture]
    public class LogicTests
    {
        private Mock<IUnitOfWork> mockedUnitOfWork;
        private Mock<IRepository<TestType>> mockedRepository;
        private Logic<TestType> logic;

        /// <summary>
        /// Setup before tests can begin
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            // Create a mocked unit of work instance
            this.mockedUnitOfWork = new Mock<IUnitOfWork>();

            // Create a mocked repository
            this.mockedRepository = new Mock<IRepository<TestType>>();

            TestType[] testObjects = new[]
            {
                new TestType { Id = 1, Text = "a", Date = DateTime.Parse("2011.01.01.") },
                new TestType { Id = 2, Text = "b", Date = DateTime.Parse("2012.02.02.") },
                new TestType { Id = 3, Text = "c", Date = DateTime.Parse("2013.03.03.") },
                new TestType { Id = 4, Text = "d", Date = DateTime.Parse("2014.04.04.") }
            };

            // Setup mock behaviour
            this.mockedUnitOfWork.Setup(m => m.GetRepository<TestType>()).Returns(this.mockedRepository.Object);

            this.mockedRepository.Setup(m => m.All()).Returns(testObjects.AsQueryable());

            this.mockedRepository.Setup(m => m.Get(x => x.Id.Equals(1))).Returns(testObjects.Where(x => x.Id.Equals(1)).AsQueryable);
            this.mockedRepository.Setup(m => m.Get(x => x.Id > 0)).Returns(testObjects.Where(x => x.Id > 0).AsQueryable);
            this.mockedRepository.Setup(m => m.Get(x => x.Id.Equals(4))).Returns(testObjects.Where(x => false).AsQueryable);

            // Create logic instance
            this.logic = new Logic<TestType>(this.mockedUnitOfWork.Object);
        }

        /// <summary>
        /// Create method test on the generic logic class.
        /// </summary>
        [Test]
        public void AddMethod_IsCalled_RepositoryMethod_IsAlsoCalled()
        {
            // Act
            this.logic.Add(new TestType());

            // Assert
            this.mockedRepository.Verify(x => x.Add(It.IsAny<TestType>()), Times.Once);
        }

        /// <summary>
        /// Read method test on the generic logic class.
        /// </summary>
        [Test]
        public void AllMethod_IsCalled_RepositoryMethod_IsAlsoCalled()
        {
            // Act
            this.logic.All();

            // Assert
            this.mockedRepository.Verify(x => x.All(), Times.Once);
        }

        /// <summary>
        /// Read method test on the generic logic class.
        /// </summary>
        /// <param name="expected">The expected type</param>
        [TestCase(typeof(IEnumerable<TestType>))]
        public void AllMethod_Returns_IEnumerableCollectionOf_GenericTypeObjects(Type expected)
        {
            // Act
            var result = this.logic.All();

            // Assert
            Assert.That(result, Is.InstanceOf(expected));
        }

        /// <summary>
        /// Read method test on the generic logic class.
        /// </summary>
        /// <param name="expected">The expected value.</param>
        [TestCase(4)]
        public void AllMethod_Returns_IEnumerableCollectionOf_Four_GenericTypeObjects(int expected)
        {
            // Act
            var result = this.logic.All().Count;

            // Assert
            Assert.That(result, Is.EqualTo(expected));
        }

        /// <summary>
        /// Delete method test on the generic logic class.
        /// </summary>
        [Test]
        public void DeleteMethod_IsCalled_RepositoryMethod_IsAlsoCalled()
        {
            // Act
            this.logic.Delete(new TestType());

            // Assert
            this.mockedRepository.Verify(x => x.Delete(It.IsAny<TestType>()), Times.Once);
        }

        /// <summary>
        /// Get method test on the generic logic class.
        /// </summary>
        [Test]
        public void GetMethod_IsCalled_RepositoryMethod_IsAlsoCalled()
        {
            // Act
            this.logic.Get(item => true);

            // Assert
            this.mockedRepository.Verify(x => x.Get(It.IsAny<Expression<Func<TestType, bool>>>()), Times.Once);
        }

        /// <summary>
        /// Get method test on the generic logic class.
        /// </summary>
        /// <param name="expected">Expected value</param>
        [TestCase(1)]
        public void GetMethod_CalledFor_SingleValue_Returns_SingleValue(int expected)
        {
            // Act
            var result = this.logic.Get(x => x.Id.Equals(1)).Count;

            // Assert
            Assert.That(result, Is.EqualTo(expected));
        }

        /// <summary>
        /// Get method test on the generic logic class.
        /// </summary>
        /// <param name="expected">Expected value</param>
        [TestCase(2)]
        public void GetMethod_CalledFor_MultipleValues_Returns_MultipleValue(int expected)
        {
            // Act
            var result = this.logic.Get(x => x.Id > 0).Count;

            // Assert
            Assert.That(result, Is.GreaterThan(1));
        }

        /// <summary>
        /// Get method test on the generic logic class.
        /// </summary>
        /// <param name="expected">Expected value</param>
        [TestCase(0)]
        public void GetMethod_CalledForWrongValue_ReturnsZero(int expected)
        {
            // Act
            var result = this.logic.Get(x => x.Id.Equals(4)).Count;

            // Assert
            Assert.That(result, Is.EqualTo(expected));
        }

        /// <summary>
        /// Update method test on the generic logic class.
        /// </summary>
        [Test]
        public void UpdateMethod_IsCalled_RepositoryMethod_IsAlsoCalled()
        {
            // Act
            this.logic.Update(new TestType());

            // Assert
            this.mockedRepository.Verify(x => x.Update(It.IsAny<TestType>()), Times.Once);
        }
    }
}