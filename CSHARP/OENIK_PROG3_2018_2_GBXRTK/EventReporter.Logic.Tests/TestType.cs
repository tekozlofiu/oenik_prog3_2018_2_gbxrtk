﻿// <copyright file="TestType.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EventReporter.Logic.Tests
{
    using System;

    /// <summary>
    /// Dummy class for testing
    /// </summary>
    public class TestType
    {
        /// <summary>
        /// Gets or sets Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets Text
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets Date
        /// </summary>
        public DateTime Date { get; set; }
    }
}