﻿CREATE TABLE [dbo].[Helyszin] (
 [Id] INT NOT NULL IDENTITY (1, 1),
 [Nev] VARCHAR (24) NOT NULL,
 [Iranyitoszam] INT NOT NULL,
 [Telepules] VARCHAR (24) NOT NULL,
 [Cim] VARCHAR (24) NOT NULL,
 [Hazszam] INT NOT NULL,
 CONSTRAINT [HelyszinPK] PRIMARY KEY CLUSTERED ([Id] ASC),
 UNIQUE NONCLUSTERED ([Nev] ASC)
);

INSERT INTO [dbo].[Helyszin]
([Nev], [Iranyitoszam], [Telepules], [Cim], [Hazszam])
VALUES 
('10minutes Café', '1092', 'Budapest', 'Futó u.', '47'),
('Barcraft Nyugati', '1188', 'Budapest', 'Bajcsy-Zsilinszky út', '58'),
('Sárkánytűz', '1092', 'Budapest', 'Ferenc krt.', '40'),
('Gamer Box', '3530', 'Miskolc', 'Széchenyi utca', '78'),
('Camelot Kártyavár', '7814', 'Eger', 'Kossuth tér', '8');

CREATE TABLE [dbo].[Jatekos] (
 [Id] INT NOT NULL IDENTITY(1,1),
 [Vezeteknev] VARCHAR (24) NOT NULL,
 [Keresztnev] VARCHAR (24) NOT NULL,
 [Becenev] VARCHAR (24) NOT NULL,
 [Regisztracio] DATE NOT NULL,
 [Szuletes] DATE NOT NULL,
 CONSTRAINT [JatekosPK] PRIMARY KEY CLUSTERED ([Id] ASC)
);

INSERT INTO [dbo].[Jatekos] 
([Becenev], [Vezeteknev], [Keresztnev], [Regisztracio], [Szuletes]) 
VALUES 
('Gambit', 'Kriston', 'Dávid', '2009-06-06', '1981-01-01'),
('Zombee', 'Nagy', 'Péter', '2010-07-07', '1981-01-02'),
('Aquir', 'Magyar', 'Máté', '2015-08-08', '1983-03-03'),
('Kósza', 'Kovács', 'Szabolcs', '2016-09-09', '1984-04-04'),
('Robin', 'Kiss', 'Robin', '2018-06-07', '1985-05-05');

CREATE TABLE [dbo].[Szervezo] (
 [Id] INT NOT NULL IDENTITY(1,1),
 [Vezeteknev] VARCHAR (24) NOT NULL,
 [Keresztnev] VARCHAR (24) NOT NULL,
 [Becenev] VARCHAR (24) NULL, 
 [Email] VARCHAR (24) NOT NULL,
 [Telefon] INT NOT NULL,
 CONSTRAINT [SzervezoPK] PRIMARY KEY CLUSTERED ([Id] ASC)
);

INSERT INTO [dbo].[Szervezo]
([Vezeteknev], [Keresztnev], [Becenev], [Email], [Telefon])
VALUES 
('Horváth', 'Tamás', 'Cjendango', 'cjendango@gmail.com', '704445566'),
('Kiss', 'Attila', 'Atus', 'attila93@gmail.com', '305647585'),
('Varga', 'Tamás', 'Vétomi', 'vtomi777@freEmail.hu', '705688712'),
('Mészáros', 'Krisztán', 'Chris', 'kirszm@gmail.com', '205457885'),
('Nagy', 'Jenő', 'Tékozlófiú', 'tekozlofiu@hotmail.com', '0613975547');

CREATE TABLE [dbo].[Verseny] (
 [Id] INT NOT NULL IDENTITY(1,1),
 [Szervezo] INT NOT NULL,
 [Helyszin] INT NOT NULL,
 [Idopont] DATE NOT NULL,
 CONSTRAINT [VersenyPK] PRIMARY KEY CLUSTERED ([Id] ASC),
 CONSTRAINT [SzervezoFK] FOREIGN KEY ([Szervezo]) REFERENCES [dbo].[Szervezo] ([Id]),
 CONSTRAINT [HelyszinFK] FOREIGN KEY ([Helyszin]) REFERENCES [dbo].[Helyszin] ([Id])
);

INSERT INTO [dbo].[Verseny]
([Szervezo], [Helyszin], [Idopont])
VALUES 
(1, 1, '2016-01-01'),
(2, 2, '2016-02-01'),
(3, 3, '2016-03-01'),
(4, 4, '2016-04-02'),
(5, 5, '2016-07-12'),
(1, 3, '2016-08-31'),
(2, 3, '2018-09-21');

CREATE TABLE [dbo].[Merkozes] (
 [Id] INT NOT NULL IDENTITY (1, 1),
 [Verseny] INT NOT NULL,
 [Gyoztes] INT NOT NULL,
 [Vesztes] INT NOT NULL,
 CONSTRAINT [MerkozesPK] PRIMARY KEY CLUSTERED ([Id] ASC),
 CONSTRAINT [VersenyFK] FOREIGN KEY ([Verseny]) REFERENCES [dbo].[Verseny] ([Id]),
 CONSTRAINT [GyoztesFK] FOREIGN KEY ([Gyoztes]) REFERENCES [dbo].[Jatekos] ([Id]),
 CONSTRAINT [VesztesFK] FOREIGN KEY ([Vesztes]) REFERENCES [dbo].[Jatekos] ([Id])
);

INSERT INTO [dbo].[Merkozes]
([Verseny], [Gyoztes], [Vesztes])
VALUES 
(1, 1, 2), (1, 3, 4), (1, 2, 4), (1, 1, 3),
(2, 5, 3),
(3, 4, 2),
(4, 3, 1), (4, 2, 5), (4, 2, 3), (4, 1, 5),
(5, 1, 5), (5, 4, 2), (5, 5, 1), (5, 4, 2),
(6, 3, 4), (6, 4, 3),
(7, 1, 2), (7, 3, 4), (7, 2, 5), (7, 2, 4), (7, 1, 3), (7, 2, 5), (7, 4, 2);

