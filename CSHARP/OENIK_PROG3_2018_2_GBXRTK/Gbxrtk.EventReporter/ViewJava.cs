﻿// <copyright file="ViewJava.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EventReporter.Program
{
    using System;
    using System.Linq;
    using EventReporter.Data;
    using EventReporter.Logic;
    using EventReporter.Repository;
    using Gbxrtk.EventReporter;

    /// <summary>
    /// View class for displaying menu options and interacting with the user.
    /// </summary>
    public class ViewJava
    {
        private static Random rng = new Random();
        private readonly string title;
        private readonly IUnitOfWork unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewJava"/> class.
        /// </summary>
        /// <param name="title">Menu title</param>
        /// <param name="unitOfWork">Unit of Work</param>
        public ViewJava(string title, IUnitOfWork unitOfWork)
        {
            this.title = title;
            this.unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Display menu options.
        /// </summary>
        /// <returns>Next action response</returns>
        public Response DisplayOptions()
        {
            var szervezok = new Logic<Szervezo>(this.unitOfWork).All().Select(x => x.Id).ToList();
            var szervezo = szervezok[rng.Next(szervezok.Count)];

            var helyszinek = new Logic<Szervezo>(this.unitOfWork).All().Select(x => x.Id).ToList();
            var helyszin = helyszinek[rng.Next(helyszinek.Count)];

            try
            {
                Java.Connect($"http://localhost:8080/OENIK_PROG3_2018_2_GBXRTK/VersenyEredmenyJelento?szervezo={szervezo}&helyszin={helyszin}", this.unitOfWork);
            }
            catch (System.Net.WebException)
            {
                Console.WriteLine("Kiszolgáló nem elérhető");
                Console.ReadKey();
                return new Response("Menü", "DisplayOptions");
            }

            return new Response("Versenyek", "Listázás");
        }
    }
}
