﻿// <copyright file="ViewJatekos.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EventReporter.Program
{
    using System;
    using System.Linq;
    using EventReporter.Data;
    using EventReporter.Logic;
    using EventReporter.Repository;
    using Gbxrtk.EventReporter;

    /// <summary>
    /// Entity specific implementation of the generic view class.
    /// </summary>
    public class ViewJatekos : View<Jatekos>
    {
        private readonly IUnitOfWork unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewJatekos"/> class.
        /// </summary>
        /// <param name="title">Title to be dispalyed in the menu</param>
        /// <param name="unitOfWork">Unit of Work</param>
        public ViewJatekos(string title, IUnitOfWork unitOfWork)
            : base(title, unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Handles the delete an entity menu option.
        /// </summary>
        /// <returns>The next action</returns>
        public override Response Törlés()
        {
            Jatekos selected;

            try
            {
                selected = UserInput.Select("Törlés", this.Logic.All());
            }
            catch (OperationCanceledException)
            {
                return new Response(this.Title, "DisplayOptions");
            }

            // Confirm the delete
            if (UserInput.Bool("Biztos törölni akarod?"))
            {
                var merkozesLogic = new Logic<Merkozes>(this.unitOfWork);
                var merkozesek = merkozesLogic.Get(x => x.Gyoztes.Equals(selected.Id) || x.Vesztes.Equals(selected.Id));

                foreach (var merkozes in merkozesek)
                {
                    merkozesLogic.Delete(merkozes);
                }

                this.Logic.Delete(this.Logic.All().Where(x => x.Equals(selected)).SingleOrDefault());
            }

            return new Response(this.Title, "Törlés");
        }
    }
}
