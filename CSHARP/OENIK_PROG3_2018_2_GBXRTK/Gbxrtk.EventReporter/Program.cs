﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EventReporter.Program
{
    using System.Collections.Generic;
    using EventReporter.Data;
    using EventReporter.Repository;
    using Gbxrtk.EventReporter;

    /// <summary>
    /// Console application
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Console application entry point
        /// </summary>
        public static void Main()
        {
            IUnitOfWork unitOfWork = new UnitOfWork<EventReporterContext>();

            Dictionary<string, object> views = new Dictionary<string, object>
            {
                { "Menü", new ViewMenu("Menü") },
                { "Helyszínek", new ViewHelyszin("Helyszínek", unitOfWork) },
                { "Játékosok", new ViewJatekos("Játékosok", unitOfWork) },
                { "Szervezők", new ViewSzervezo("Szervezők", unitOfWork) },
                { "Versenyek", new ViewVerseny("Versenyek", unitOfWork) },
                { "Mérkőzések", new ViewMerkozes("Mérkőzések", unitOfWork) },
                { "Webes adatok", new ViewJava("Webes adatok", unitOfWork) }
            };

            Response request = new Response("Menü", "DisplayOptions");

            while (true)
            {
                var view = views[request.ClassName];
                request = (Response)view.GetType().GetMethod(request.MethodName).Invoke(view, request.Parameters);
            }
        }
    }
}