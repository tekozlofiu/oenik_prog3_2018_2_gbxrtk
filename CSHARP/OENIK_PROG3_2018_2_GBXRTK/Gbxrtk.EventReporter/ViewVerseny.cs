﻿// <copyright file="ViewVerseny.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EventReporter.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using EventReporter.Data;
    using EventReporter.Logic;
    using EventReporter.Repository;
    using Gbxrtk.EventReporter;

    /// <summary>
    /// Entity specific implementation of the generic view class.
    /// </summary>
    public class ViewVerseny : View<Verseny>
    {
        private readonly IUnitOfWork unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewVerseny"/> class.
        /// </summary>
        /// <param name="title">Title to be displayed</param>
        /// <param name="unitOfWork">Unit of Work</param>
        public ViewVerseny(string title, IUnitOfWork unitOfWork)
            : base(title, unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Handles the add new entitya menu option.
        /// </summary>
        /// <returns>Next action response</returns>
        public override Response Hozzáadás()
        {
            // Create a new instance
            Verseny verseny = new Verseny();

            try
            {
                // Select a venue for the event
                verseny.Helyszin = UserInput.Select("Válassz helyszínt!", new Logic<Helyszin>(this.unitOfWork).All().ToList()).Id;

                // Select an organizer for the event
                verseny.Szervezo = UserInput.Select("Válassz szervezőt!", new Logic<Szervezo>(this.unitOfWork).All().ToList()).Id;
            }
            catch (OperationCanceledException)
            {
                return new Response(this.Title, "DisplayOptions");
            }

            UserInput.Title("Add meg a verseny időpontját!");

            // Set the date of the event
            verseny.Idopont = UserInput.Date("Időpont");

            // Add the entity to the repository
            this.Logic.Add(verseny);

            // Confirm the user
            Console.WriteLine("\nAz új verseny hozzáadva.");
            Console.ReadKey();

            // Return for next action
            return new Response(this.Title, "DisplayOptions");
        }

        /// <summary>
        /// Handles the delete an entity menu option.
        /// </summary>
        /// <returns>The next action</returns>
        public override Response Törlés()
        {
            Verseny selected;

            try
            {
                selected = UserInput.Select("Törlés", this.Logic.All());
            }
            catch (OperationCanceledException)
            {
                return new Response(this.Title, "DisplayOptions");
            }

            // Confirm the delete
            if (UserInput.Bool("Biztos törölni akarod?"))
            {
                var logic = new Logic<Merkozes>(this.unitOfWork);
                var mérkőzések = logic.Get(x => x.Verseny.Equals(selected.Id));
                foreach (var item in mérkőzések)
                {
                    logic.Delete(item);
                }

                this.Logic.Delete(this.Logic.All().Where(x => x.Equals(selected)).SingleOrDefault());
            }

            return new Response(this.Title, "Törlés");
        }

        /// <summary>
        /// Handles the modify menu option.
        /// </summary>
        /// <returns>The next action</returns>
        public override Response Módosítás()
        {
            // Select an entity
            Verseny selectedEntity;

            try
            {
                selectedEntity = UserInput.Select(this.Title, this.Logic.All());
            }
            catch (OperationCanceledException)
            {
                return new Response(this.Title, "DisplayOptions");
            }

            // Select a property
            string selectedProperty;

            try
            {
                selectedProperty = UserInput.Select(this.Title, new List<string>() { "Szervező", "Helyszin", "Időpont" });
            }
            catch (OperationCanceledException)
            {
                return new Response(this.Title, "DisplayOptions");
            }

            switch (selectedProperty)
            {
                case "Szervező":
                    try
                    {
                        selectedEntity.Szervezo = UserInput.Select("Válassz szervezőt!", new Logic<Szervezo>(this.unitOfWork).All()).Id;
                    }
                    catch (OperationCanceledException)
                    {
                        return new Response(this.Title, "DisplayOptions");
                    }

                    break;
                case "Helyszín":
                    try
                    {
                        selectedEntity.Helyszin = UserInput.Select("Válassz helyszínt!", new Logic<Helyszin>(this.unitOfWork).All()).Id;
                    }
                    catch (OperationCanceledException)
                    {
                        return new Response(this.Title, "DisplayOptions");
                    }

                    break;
                default:
                    selectedEntity.Idopont = UserInput.Date("Időpont");
                    break;
            }

            try
            {
                this.Logic.Update(selectedEntity);
            }
            catch (Exception)
            {
                Console.WriteLine("A módosítás nem sikerült. Az adatok hibásak.");
                Console.ReadKey();
                return new Response(this.Title, MethodBase.GetCurrentMethod().Name);
            }

            Console.WriteLine("\nMódosítva");
            Console.ReadKey();

            return new Response(this.Title, "Módosítás");
        }
    }
}
