﻿// <copyright file="ViewMenu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EventReporter.Program
{
    using System;
    using System.Collections.Generic;
    using Gbxrtk.EventReporter;

    /// <summary>
    /// View class for displaying menu options and interacting with the user.
    /// </summary>
    public class ViewMenu
    {
        private readonly string title;
        private readonly List<string> options = new List<string> { "Helyszínek", "Játékosok", "Szervezők", "Versenyek", "Mérkőzések", "Webes adatok" };

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewMenu"/> class.
        /// </summary>
        /// <param name="title">Title to be displayed in the menu.</param>
        public ViewMenu(string title)
        {
            this.title = title;
        }

        /// <summary>
        /// Displays menu options.
        /// </summary>
        /// <returns>Next action response.</returns>
        public Response DisplayOptions()
        {
            string selected;

            try
            {
               selected = UserInput.Select(this.title, this.options);
            }
            catch (OperationCanceledException)
            {
                return new Response("Menü", "DisplayOptions");
            }

            return new Response(selected, "DisplayOptions");
        }
    }
}
