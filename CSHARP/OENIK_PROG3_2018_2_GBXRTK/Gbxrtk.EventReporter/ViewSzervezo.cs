﻿// <copyright file="ViewSzervezo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EventReporter.Program
{
    using System;
    using System.Linq;
    using EventReporter.Data;
    using EventReporter.Logic;
    using EventReporter.Repository;
    using Gbxrtk.EventReporter;

    /// <summary>
    /// Entity specific implementation of the generic view class.
    /// </summary>
    public class ViewSzervezo : View<Szervezo>
    {
        private readonly IUnitOfWork unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewSzervezo"/> class.
        /// </summary>
        /// <param name="title">Title to be displayed in the menu.</param>
        /// <param name="unitOfWork">Unit of work.</param>
        public ViewSzervezo(string title, IUnitOfWork unitOfWork)
            : base(title, unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Handles the delete an entity menu option.
        /// </summary>
        /// <returns>The next action</returns>
        public override Response Törlés()
        {
            Szervezo selected;

            try
            {
                selected = UserInput.Select("Törlés", this.Logic.All());
            }
            catch (OperationCanceledException)
            {
                return new Response(this.Title, "DisplayOptions");
            }

            // Confirm the delete
            if (UserInput.Bool("Biztos törölni akarod?"))
            {
                var versenyLogic = new Logic<Verseny>(this.unitOfWork);
                var merkozesLogic = new Logic<Merkozes>(this.unitOfWork);
                var versenyek = versenyLogic.Get(x => x.Szervezo.Equals(selected.Id));

                foreach (var verseny in versenyek)
                {
                    var merkozesek = merkozesLogic.Get(x => x.Verseny.Equals(verseny.Id));

                    foreach (var merkozes in merkozesek)
                    {
                        merkozesLogic.Delete(merkozes);
                    }

                    versenyLogic.Delete(verseny);
                }

                this.Logic.Delete(this.Logic.All().Where(x => x.Equals(selected)).SingleOrDefault());
            }

            return new Response(this.Title, "Törlés");
        }
    }
}
