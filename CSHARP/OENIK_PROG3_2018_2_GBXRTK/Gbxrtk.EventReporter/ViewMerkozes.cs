﻿// <copyright file="ViewMerkozes.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EventReporter.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using EventReporter.Data;
    using EventReporter.Logic;
    using EventReporter.Repository;
    using Gbxrtk.EventReporter;

    /// <summary>
    /// Entity specific implementation of the generic view class.
    /// </summary>
    public class ViewMerkozes : View<Merkozes>
    {
        private readonly IUnitOfWork unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewMerkozes"/> class.
        /// </summary>
        /// <param name="title">Title to be displayed in the menu</param>
        /// <param name="unitOfWork">Unit of work</param>
        public ViewMerkozes(string title, IUnitOfWork unitOfWork)
            : base(title, unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Handles the add new entity menu option.
        /// </summary>
        /// <returns>Next action response.</returns>
        public override Response Hozzáadás()
        {
            // Create a new instance
            Merkozes verseny = new Merkozes();

            try
            {
                // Select a tournament for the event
                verseny.Verseny = UserInput.Select("Válassz versenyt!", new Logic<Verseny>(this.unitOfWork).All().ToList()).Id;

                // Select the winner
                verseny.Gyoztes = UserInput.Select("Válaszd ki a győztest!", new Logic<Jatekos>(this.unitOfWork).All().ToList()).Id;

                // Select the loser
                verseny.Vesztes = UserInput.Select("Válaszd ki a vesztest!", new Logic<Jatekos>(this.unitOfWork).All().ToList()).Id;
            }
            catch (OperationCanceledException)
            {
                return new Response(this.Title, "DisplayOptions");
            }

            // Add the entity to the repository
            this.Logic.Add(verseny);

            // Confirm the user
            Console.WriteLine("\nAz eredmény hozzáadva.");
            Console.ReadKey();

            // Return for next action
            return new Response(this.Title, "DisplayOptions");
        }

        /// <summary>
        /// Hadnles the display details page.
        /// </summary>
        /// <param name="single">Entity to be displayed.</param>
        /// <returns>Next action response.</returns>
        public override Response Részletek(object single)
        {
            Merkozes meccs = (Merkozes)single;

            var jatekos = this.unitOfWork.GetRepository<Jatekos>().All();

            var q = (from merkozes in this.Logic.Get(x => x.Id.Equals(meccs.Id))
                    join gyoztes in new Logic<Jatekos>(this.unitOfWork).All() on meccs.Gyoztes equals gyoztes.Id
                    join vesztes in new Logic<Jatekos>(this.unitOfWork).All() on meccs.Vesztes equals vesztes.Id
                    join verseny in new Logic<Verseny>(this.unitOfWork).All() on meccs.Verseny equals verseny.Id
                    join helyszin in new Logic<Helyszin>(this.unitOfWork).All() on verseny.Helyszin equals helyszin.Id
                    join szervezo in new Logic<Szervezo>(this.unitOfWork).All() on verseny.Szervezo equals szervezo.Id
                    select new
                    {
                        Helyszin = helyszin.Nev,
                        Szervezo = szervezo.Becenev,
                        verseny.Idopont,
                        Gyoztes = gyoztes.Becenev,
                        Vesztes = vesztes.Becenev,
                    }).First();

            UserInput.Title("A mérkőzés adatai");

            Console.WriteLine(
                "  Helyszín: " + q.Helyszin + "\n" +
                "  Szervező: " + q.Szervezo + "\n" +
                "  Időpont:  " + q.Idopont.ToString("yyyy-MM-dd") + "\n" +
                "  Győztes:  " + q.Gyoztes + "\n" +
                "  Vesztes:  " + q.Vesztes);

            Console.ReadKey();

            return new Response(this.Title, "DisplayOptions");
        }

        /// <summary>
        /// Handles the modify menu option.
        /// </summary>
        /// <returns>Next action response.</returns>
        public override Response Módosítás()
        {
            // Select an entity
            Merkozes selectedEntity;

            try
            {
                selectedEntity = UserInput.Select(this.Title, this.Logic.All());
            }
            catch (OperationCanceledException)
            {
                return new Response(this.Title, "DisplayOptions");
            }

            // Select a property
            string selectedProperty;

            try
            {
                selectedProperty = UserInput.Select(this.Title, new List<string>() { "Győztes", "Vesztes", "Verseny" });
            }
            catch (OperationCanceledException)
            {
                return new Response(this.Title, "DisplayOptions");
            }

            switch (selectedProperty)
            {
                case "Győztes":
                    try
                    {
                        int id = UserInput.Select(this.Title, new Logic<Jatekos>(this.unitOfWork).All()).Id;
                        selectedEntity.Gyoztes = id;
                    }
                    catch (OperationCanceledException)
                    {
                        return new Response(this.Title, "DisplayOptions");
                    }

                    break;
                case "Vesztes":
                    try
                    {
                        int id = UserInput.Select(this.Title, new Logic<Jatekos>(this.unitOfWork).All()).Id;
                        selectedEntity.Vesztes = id;
                    }
                    catch (OperationCanceledException)
                    {
                        return new Response(this.Title, "DisplayOptions");
                    }

                    break;
                default:
                    try
                    {
                        int id = UserInput.Select(this.Title, new Logic<Verseny>(this.unitOfWork).All()).Id;
                        selectedEntity.Verseny = id;
                    }
                    catch (OperationCanceledException)
                    {
                        return new Response(this.Title, "DisplayOptions");
                    }

                    break;
            }

            try
            {
                this.Logic.Update(selectedEntity);
            }
            catch (Exception)
            {
                Console.WriteLine("A módosítás nem sikerült. Az adatok hibásak.");
                Console.ReadKey();
                return new Response(this.Title, MethodBase.GetCurrentMethod().Name);
            }

            Console.WriteLine("\nMódosítva");
            Console.ReadKey();

            return new Response(this.Title, "Módosítás");
        }
    }
}