﻿// <copyright file="UserInput.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EventReporter.Program
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// A collection of static methods for displaying information and getting manual input from the user.
    /// </summary>
    public static class UserInput
    {
        private static readonly string Padding = "    ";

        /// <summary>
        /// Get a string type value from the user.
        /// </summary>
        /// <param name="message">Message to be displayed for the user.</param>
        /// <returns>String type value given by the user.</returns>
        public static string String(string message)
        {
            Console.CursorVisible = true;

            string input;

            do
            {
                Console.Write(message + ": ");
                input = Console.ReadLine();
            }
            while (input.Length < 1);

            return input;
        }

        /// <summary>
        /// Get an integer type value from the user.
        /// </summary>
        /// <param name="message">Message to be displayed for the user.</param>
        /// <returns>Integer type value given by the user.</returns>
        public static int Integer(string message)
        {
            Console.CursorVisible = true;

            string input;
            int output;

            do
            {
                Console.Write(message + ": ");
                input = Console.ReadLine();
            }
            while (!int.TryParse(input, out output));

            return output;
        }

        /// <summary>
        /// Get a date type value from the user.
        /// </summary>
        /// <param name="message">Message to be displayed for the user.</param>
        /// <returns>Date type value given by the user.</returns>
        public static DateTime Date(string message)
        {
            Console.CursorVisible = true;

            string input;
            DateTime date;

            do
            {
                Console.Write(message + ": ");
                input = Console.ReadLine();
            }
            while (!DateTime.TryParse(input, out date));

            return date;
        }

        /// <summary>
        /// Select a value from a list of options.
        /// </summary>
        /// <typeparam name="T">Generic type</typeparam>
        /// <param name="title">A title to be shown on the console.</param>
        /// <param name="options">An array containg the possible options.</param>
        /// <returns>The selected option.</returns>
        public static T Select<T>(string title, List<T> options)
        {
            ConsoleKeyInfo input;
            int selected = 0;

            do
            {
                Console.CursorVisible = false;

                Title(title);

                if (selected == options.Count)
                {
                    HighlightOption("Vissza");
                }
                else
                {
                    DisplayOption("Vissza");
                }

                for (int i = 0; i < options.Count; i++)
                {
                    if (i == selected)
                    {
                        HighlightOption(options[i].ToString());
                    }
                    else
                    {
                        DisplayOption(options[i].ToString());
                    }
                }

                input = Console.ReadKey();

                switch (input.Key)
                {
                    case ConsoleKey.UpArrow:
                        selected = (selected == 0) ? options.Count : selected - 1;
                        break;
                    case ConsoleKey.DownArrow:
                        selected = (selected == options.Count) ? 0 : selected + 1;
                        break;
                    default:
                        break;
                }
            }
            while (input.Key != ConsoleKey.Enter);

            if (selected == options.Count)
            {
                throw new OperationCanceledException();
            }

            return options[selected];
        }

        /// <summary>
        /// Select yes or no.
        /// </summary>
        /// <param name="title">A title to be shown on the console.</param>
        /// <returns>The selected option.</returns>
        public static bool Bool(string title)
        {
            ConsoleKeyInfo input;
            bool selected = false;

            do
            {
                Title(title);

                if (selected)
                {
                    HighlightOption("Igen");
                    DisplayOption("Nem");
                }
                else
                {
                    DisplayOption("Igen");
                    HighlightOption("Nem");
                }

                input = Console.ReadKey();

                if (input.Key == ConsoleKey.UpArrow || input.Key == ConsoleKey.DownArrow)
                {
                    selected = !selected;
                }
            }
            while (input.Key != ConsoleKey.Enter);

            return selected;
        }

        /// <summary>
        /// Displays a formated title message on the console
        /// </summary>
        /// <param name="title">The text to be displayed</param>
        public static void Title(string title)
        {
            Console.Clear();
            Console.WriteLine("  --==  {0}  ==--  \n", title);
        }

        /// <summary>
        /// Display a menu option
        /// </summary>
        /// <param name="text">The menu option to be displayed</param>
        private static void DisplayOption(string text)
        {
            Console.WriteLine("{0}{1}", Padding, text);
        }

        /// <summary>
        /// Display a menu option in with highlight
        /// </summary>
        /// <param name="text">The menu option to be displayed</param>
        private static void HighlightOption(string text)
        {
            Console.Write(Padding);
            Console.BackgroundColor = ConsoleColor.Gray;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Write(text + "\n");
            Console.ResetColor();
        }
    }
}