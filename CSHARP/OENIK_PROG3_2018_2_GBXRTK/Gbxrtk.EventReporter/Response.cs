﻿// <copyright file="Response.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Gbxrtk.EventReporter
{
    /// <summary>
    /// Represents the next action to be executed by the menu.
    /// </summary>
    public class Response
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Response"/> class.
        /// </summary>
        /// <param name="className">Name of the class to be invoked.</param>
        /// <param name="methodName">Name of the methods to be invoked</param>
        public Response(string className, string methodName)
        {
            this.ClassName = className;
            this.MethodName = methodName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Response"/> class.
        /// </summary>
        /// <param name="className">Name of the class to be invoked.</param>
        /// <param name="methodName">Name of the methods to be invoked.</param>
        /// <param name="parameter">A single object parameter to be passed for the method.</param>
        public Response(string className, string methodName, object parameter)
        {
            this.ClassName = className;
            this.MethodName = methodName;
            this.Parameters = new object[] { parameter };
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Response"/> class.
        /// </summary>
        /// <param name="className">Name of the class to be invoked.</param>
        /// <param name="methodName">Name of the methods to be invoked.</param>
        /// <param name="parameters">An array of object parameters to be passed for the method.</param>
        public Response(string className, string methodName, object[] parameters)
        {
            this.ClassName = className;
            this.MethodName = methodName;
            this.Parameters = parameters;
        }

        /// <summary>
        /// Gets the class name.
        /// </summary>
        public string ClassName { get; }

        /// <summary>
        /// Gets the method name.
        /// </summary>
        public string MethodName { get; }

        /// <summary>
        /// Gets the paramters.
        /// </summary>
        public object[] Parameters { get; }
    }
}