﻿// <copyright file="View.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EventReporter.Program
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Reflection;
    using EventReporter.Logic;
    using EventReporter.Repository;
    using Gbxrtk.EventReporter;

    /// <summary>
    /// A generic class responsible for displaying menu options and interacting with the user.
    /// </summary>
    /// <typeparam name="TModel">Data model type</typeparam>
    public class View<TModel>
        where TModel : class
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="View{TModel}"/> class.
        /// </summary>
        /// <param name="title">Title of the menu</param>
        /// <param name="unitOfWork">Unit of Work</param>
        public View(string title, IUnitOfWork unitOfWork)
        {
            this.Title = title;
            this.Options = new List<string> { "Hozzáadás", "Listázás", "Törlés", "Módosítás" };
            this.Logic = new Logic<TModel>(unitOfWork);
            this.Properties = typeof(TModel).GetProperties().Where(x => x.GetCustomAttributes(typeof(RequiredAttribute)).Count() > 0).ToList();
        }

        /// <summary>
        /// Gets the title.
        /// </summary>
        protected string Title { get; }

        /// <summary>
        /// Gets the menu options.
        /// </summary>
        protected List<string> Options { get; }

        /// <summary>
        /// Gets the logic class for the model.
        /// </summary>
        protected ILogic<TModel> Logic { get; }

        /// <summary>
        /// Gets the properties of the model.
        /// </summary>
        protected List<PropertyInfo> Properties { get; }

        /// <summary>
        /// Displays the options available in the menu.
        /// </summary>
        /// <returns>The next action.</returns>
        public Response DisplayOptions()
        {
            string selected;

            try
            {
                selected = UserInput.Select(this.Title, this.Options);
            }
            catch (OperationCanceledException)
            {
                return new Response("Menü", "DisplayOptions");
            }

            return new Response(this.Title, selected);
        }

        /// <summary>
        /// Displays a list of entities.
        /// </summary>
        /// <returns>The next action.</returns>
        public Response Listázás()
        {
            TModel selected;

            try
            {
                selected = UserInput.Select(this.Title, this.Logic.All());
            }
            catch (OperationCanceledException)
            {
                return new Response(this.Title, "DisplayOptions");
            }

            return new Response(this.Title, "Részletek", selected);
        }

        /// <summary>
        /// Displays detailed information about a single entitly.
        /// </summary>
        /// <param name="single">The entity.</param>
        /// <returns>The next action</returns>
        public virtual Response Részletek(object single)
        {
            Console.Clear();
            Console.WriteLine("  --==  {0}  ==--  \n", this.Title);

            TModel entity = (TModel)single;

            foreach (var property in this.Properties)
            {
                if (property.PropertyType == typeof(DateTime))
                {
                    DateTime date = (DateTime)property.GetValue(entity);
                    Console.WriteLine("    {0}: {1}", property.GetCustomAttribute<DisplayAttribute>().Name, date.ToString("yyyy-MM-dd"));
                }
                else
                {
                    Console.WriteLine("    {0}: {1}", property.GetCustomAttribute<DisplayAttribute>().Name, property.GetValue(entity).ToString());
                }
            }

            Console.ReadKey();

            return new Response(this.Title, "Listázás");
        }

        /// <summary>
        /// Handles the add new entity menu option.
        /// </summary>
        /// <returns>The next action</returns>
        public virtual Response Hozzáadás()
        {
            TModel newEntity = (TModel)Activator.CreateInstance(typeof(TModel));

            Console.Clear();
            Console.WriteLine("  --==  {0}  ==--  \n", this.Title);

            // For each property get a user input
            foreach (var property in this.Properties)
            {
                object userinput;
                string message = property.GetCustomAttribute<DisplayAttribute>().Name;

                if (property.PropertyType == typeof(int))
                {
                    userinput = UserInput.Integer(message);
                }
                else if (property.PropertyType == typeof(DateTime))
                {
                    userinput = UserInput.Date(message);
                }
                else
                {
                    userinput = UserInput.String(message);
                }

                property.SetValue(newEntity, Convert.ChangeType(userinput, property.PropertyType), null);
            }

            try
            {
                // Add the entity
                this.Logic.Add(newEntity);
            }
            catch (Exception)
            {
                Console.WriteLine("A hozzáadás nem sikerült. Az adatok hibásak.");
                Console.ReadKey();
                return new Response(this.Title, "DisplayOptions");
            }

            Console.WriteLine("\nHozzáadva");
            Console.ReadKey();

            return new Response(this.Title, "DisplayOptions");
        }

        /// <summary>
        /// Handles the delete an entity menu option.
        /// </summary>
        /// <returns>The next action</returns>
        public virtual Response Törlés()
        {
            TModel selected;

            try
            {
                selected = UserInput.Select("Törlés", this.Logic.All());
            }
            catch (OperationCanceledException)
            {
                return new Response(this.Title, "DisplayOptions");
            }

            // Confirm the delete
            if (UserInput.Bool("Biztos törölni akarod?"))
            {
                this.Logic.Delete(this.Logic.All().Where(x => x.Equals(selected)).SingleOrDefault());
            }

            return new Response(this.Title, "Törlés");
        }

        /// <summary>
        /// Handles the modify an entity menu option.
        /// </summary>
        /// <returns>The next action</returns>
        public virtual Response Módosítás()
        {
            // Select an entity
            TModel selectedEntity;

            try
            {
                selectedEntity = UserInput.Select(this.Title, this.Logic.All());
            }
            catch (OperationCanceledException)
            {
                return new Response(this.Title, "DisplayOptions");
            }

            // Select a property
            string selectedProperty;

            try
            {
                selectedProperty = UserInput.Select(this.Title, this.Properties.Select(x => x.GetCustomAttribute<DisplayAttribute>().Name).ToList());
            }
            catch (OperationCanceledException)
            {
                return new Response(this.Title, "DisplayOptions");
            }

            PropertyInfo property = this.Properties.Where(x => x.GetCustomAttribute<DisplayAttribute>().Name == selectedProperty).First();

            Console.Clear();
            Console.WriteLine("  --==  {0}  ==--  \n", this.Title);

            // Get the new values from the user
            object userinput;
            string message = property.GetCustomAttribute<DisplayAttribute>().Name;

            if (property.PropertyType == typeof(int))
            {
                userinput = UserInput.Integer(message);
            }
            else if (property.PropertyType == typeof(DateTime))
            {
                userinput = UserInput.Date(message);
            }
            else
            {
                userinput = UserInput.String(message);
            }

            // Modify the entity with new value
            property.SetValue(selectedEntity, Convert.ChangeType(userinput, property.PropertyType), null);

            try
            {
                // Update the entity
                this.Logic.Update(selectedEntity);
            }
            catch (Exception)
            {
                Console.WriteLine("A módosítás nem sikerült. Az adatok hibásak.");
                Console.ReadKey();
                return new Response(this.Title, MethodBase.GetCurrentMethod().Name);
            }

            Console.WriteLine("\nMódosítva");
            Console.ReadKey();

            return new Response(this.Title, "Módosítás");
        }
    }
}