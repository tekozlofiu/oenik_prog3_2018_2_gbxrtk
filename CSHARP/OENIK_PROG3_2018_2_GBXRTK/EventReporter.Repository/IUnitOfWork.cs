﻿// <copyright file="IUnitOfWork.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EventReporter.Repository
{
    /// <summary>
    /// Interface for Unit Of Work pattern implementation.
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// Adds a repository to the dictionary
        /// </summary>
        /// <typeparam name="TEntity">Class of the entities stored in the repository.</typeparam>
        /// <param name="repository">A repository object where entities with the specified type are stored.</param>
        void AddRepository<TEntity>(IRepository<TEntity> repository)
            where TEntity : class;

        /// <summary>
        /// Gets the repository from a dictionary where entities with the specified type are stored.
        /// </summary>
        /// <typeparam name="TEntity">Class of the entities stored in the repository.</typeparam>
        /// <returns>The repository object where entities with the specified type are stored.</returns>
        IRepository<TEntity> GetRepository<TEntity>()
            where TEntity : class;

        /// <summary>
        /// Save changes made to the repository
        /// </summary>
        void Save();
    }
}
