﻿// <copyright file="UnitOfWork.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EventReporter.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    /// <summary>
    /// Implements the Unit Of Work pattern. Manages multiple repositories sharing the same database context.
    /// </summary>
    /// <typeparam name="TContext">Type of the database context.</typeparam>
    public class UnitOfWork<TContext> : IUnitOfWork
        where TContext : DbContext, new()
    {
        private readonly TContext context;
        private readonly Dictionary<Type, object> repositories;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWork{TContext}"/> class.
        /// </summary>
        public UnitOfWork()
        {
            this.context = (TContext)typeof(TContext).GetConstructor(Type.EmptyTypes).Invoke(null);
            this.repositories = new Dictionary<Type, object>();
        }

        /// <summary>
        /// Adds a repository to the dictionary
        /// </summary>
        /// <typeparam name="TEntity">Class of the entities stored in the repository.</typeparam>
        /// <param name="repository">A repository object where entities with the specified type are stored.</param>
        public void AddRepository<TEntity>(IRepository<TEntity> repository)
            where TEntity : class
        {
            this.repositories.Add(typeof(TEntity), repository);
        }

        /// <summary>
        /// Gets the repository from a dictionary where entities with the specified type are stored.
        /// If the repository does not exist yet, it creates a generic one and stores it in a dictionary.
        /// </summary>
        /// <typeparam name="TEntity">Class of the entities stored in the repository.</typeparam>
        /// <returns>The repository object where entities with the specified type are stored.</returns>
        public IRepository<TEntity> GetRepository<TEntity>()
            where TEntity : class
        {
            if (this.repositories.Keys.Contains(typeof(TEntity)))
            {
                return this.repositories[typeof(TEntity)] as IRepository<TEntity>;
            }

            var repository = new Repository<TEntity>(this.context);

            this.repositories.Add(typeof(TEntity), repository);

            return repository;
        }

        /// <summary>
        /// Save changes made on the repositories in the context.
        /// </summary>
        public void Save()
        {
            this.context.SaveChanges();
        }
    }
}