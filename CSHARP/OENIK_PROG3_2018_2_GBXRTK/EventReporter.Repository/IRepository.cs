﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EventReporter.Repository
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    /// <summary>
    /// Generic data access level interface. The repository must implement the basic CRUD methods (Creat, Read, Updtate and Delete).
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity to be stored in the repository.</typeparam>
    public interface IRepository<TEntity>
        where TEntity : class
    {
        /// <summary>
        /// Add a new entity to the database.
        /// </summary>
        /// <param name="entity">The new entity to be added.</param>
        void Add(TEntity entity);

        /// <summary>
        /// Fetches every entity from the database.
        /// </summary>
        /// <returns>All entities.</returns>
        IQueryable<TEntity> All();

        /// <summary>
        /// Remove an entity from the database.
        /// </summary>
        /// <param name="entity">The entity to be removed.</param>
        void Delete(TEntity entity);

        /// <summary>
        /// Fetch entities from the database that are corresponding to a given condition.
        /// </summary>
        /// <param name="condition">The condition that has to be matched.</param>
        /// <returns>Entities that are satisfing the condition.</returns>
        IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> condition);

        /// <summary>
        /// Modify an entity in the database.
        /// </summary>
        /// <param name="entity">The entity to be modified.</param>
        void Update(TEntity entity);
    }
}