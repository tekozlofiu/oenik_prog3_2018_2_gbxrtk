﻿// <copyright file="Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EventReporter.Repository
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;

    /// <summary>
    /// Implements an abstraction layer between the data access and the business logic of the application.
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity to be stored in the repository.</typeparam>
    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        private DbContext context;
        private DbSet<TEntity> dbSet;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository{T}"/> class.
        /// </summary>
        /// <param name="context">Database context.</param>
        public Repository(DbContext context)
        {
            this.context = context;
            this.dbSet = this.context.Set<TEntity>();
        }

        /// <summary>
        /// Add a new entitiy to the database.
        /// </summary>
        /// <param name="entity">The new entity to be added.</param>
        public void Add(TEntity entity)
        {
            this.dbSet.Add(entity);
        }

        /// <summary>
        /// Fetches every entity from the database.
        /// </summary>
        /// <returns>All entities.</returns>
        public IQueryable<TEntity> All()
        {
            return this.dbSet;
        }

        /// <summary>
        /// Remove an entity from the database.
        /// </summary>
        /// <param name="entity">The entity to be removed.</param>
        public void Delete(TEntity entity)
        {
            this.dbSet.Remove(entity);
        }

        /// <summary>
        /// Fetch entities from the database that are corresponding to a given condition.
        /// </summary>
        /// <param name="condition">The condition that has to be matched.</param>
        /// <returns>Entities that are satisfing the condition.</returns>
        public IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> condition)
        {
            return this.dbSet.Where(condition);
        }

        /// <summary>
        /// Modify an entity in the database.
        /// </summary>
        /// <param name="entity">The entity to be modified.</param>
        public void Update(TEntity entity)
        {
            this.dbSet.Attach(entity);
            this.context.Entry(entity).State = EntityState.Modified;
        }
    }
}