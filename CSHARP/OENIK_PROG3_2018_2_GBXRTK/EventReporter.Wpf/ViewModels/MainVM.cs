﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;

namespace EventReporter.Wpf
{
    class MainVM : ViewModelBase
    {
		private readonly MainLogic logic;
		private LocationVM selected;
		private ObservableCollection<LocationVM> locations;

		public LocationVM Selected
		{
			get => selected;
			set => Set(ref selected, value);
		}
		public ObservableCollection<LocationVM> Locations
		{
			get => locations;
			set => Set(ref locations, value);
		}
		public Func<LocationVM, bool> EditorFunc { get; set; }
		public ICommand AddCommand { get; private set; }
		public ICommand DelCommand { get; private set; }
		public ICommand ModCommand { get; private set; }
		public ICommand LoadCommand { get; private set; }

		public MainVM()
		{
			logic = new MainLogic();

			AddCommand = new RelayCommand(() => logic.EditLocation(null, EditorFunc));
			DelCommand = new RelayCommand(() => logic.ApiDeleteLocation(Selected));
			ModCommand = new RelayCommand(() => logic.EditLocation(Selected, EditorFunc));
			LoadCommand = new RelayCommand(() => Locations = new ObservableCollection<LocationVM>(logic.ApiGetLocations()));
		}
	}
}
