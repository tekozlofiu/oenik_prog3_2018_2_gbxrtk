﻿using System;
using GalaSoft.MvvmLight;

namespace EventReporter.Wpf
{
    public class LocationVM : ObservableObject
    {
        int id;
        string name;
        string city;
        int zip;
        string address;
        int number;

		public int Id 
        { 
            get => id; 
            set => Set(ref id, value); 
        }
		public string Name 
        { 
            get => name;
            set => Set(ref name, value); 
        }
		public int Zip 
        { 
            get => zip; 
            set => Set(ref zip, value); 
        }
		public string City 
        { 
            get => city; 
            set => Set(ref city, value); 
        }
		public string Address 
        { 
            get => address; 
            set => Set(ref address, value); 
        }
		public int Number 
        { 
            get => number; 
            set => Set(ref number, value);
        }

        public void CopyFrom(LocationVM other)
        {
            if(other is object)
            {
                this.Id = other.Id;
                this.Name = other.Name;
                this.Zip = other.Zip;
                this.City = other.City;
                this.Address = other.Address;
                this.Number = other.Number;
            }
        }
    }
}
