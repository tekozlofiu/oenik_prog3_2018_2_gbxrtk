﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace EventReporter.Wpf
{
    class MainLogic
    {
        private readonly string urlBase = "http://localhost:63654/api/LocationApi/";
        private readonly HttpClient client = new HttpClient();

        private void SendMessage(bool success)
        {
            string message = (success) ? "Operation completed" : "Operation failed";
            Messenger.Default.Send(message, "CarResult");
        }

        public List<LocationVM> ApiGetLocations()
        {
            string json = client.GetStringAsync(urlBase + "all").Result;
            var list = JsonConvert.DeserializeObject<List<LocationVM>>(json);
            // do NOT call the Messenger!
            return list;
        }

        public void ApiDeleteLocation(LocationVM location)
        {
            bool success = false;

            if (location is object)
            {
                string json = client.GetStringAsync(urlBase + "del/" + location.Id).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationResult"];
            }
            SendMessage(success);
        }

        public void EditLocation(LocationVM location, Func<LocationVM, bool> editor)
        {
            var clone = new LocationVM();

            if (location is object)
                clone.CopyFrom(location);

            bool? success = editor?.Invoke(clone);

            if(success == true)
            {
                success = ApiEditLocation(clone, location != null);
            }
            SendMessage(success == true);
        }

        private bool ApiEditLocation(LocationVM location, bool isEditing)
        {
            if (location == null)
                return false;

            var postData = new Dictionary<string, string>()
            {
                { nameof(LocationVM.Name), location.Name },
                { nameof(LocationVM.City), location.City },
                { nameof(LocationVM.Zip), location.Zip.ToString()},
                { nameof(LocationVM.Address), location.Address},
                { nameof(LocationVM.Number), location.Number.ToString()},
            };

            if (isEditing)
            {
                postData.Add(nameof(LocationVM.Id), location.Id.ToString());
            }

            string myUrl = (isEditing) ? urlBase + "mod" : urlBase + "add";

            string json = client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JObject obj = JObject.Parse(json);
            return (bool)obj["OperationResult"];
        }

    }
}
